# React Todo App

該專案是Fork該連結專案: https://github.com/drehimself/todo-react

## 安裝

``` bash
# install dependencies
npm install

# serve at localhost:3000
npm start
```
## 使用json-server

安裝json-server來使用fake-api

```json=
npm install json-server -g
```

然後在該專案root 執行以下指令

```shell=
json-server ./fake-api/todo.json
```

注意: json-server要啟動在 port: 3000, 否則要自己去改 `./src/stores/TodoStore.js`


